import { SimulationControllerClient } from '@sila-standard/core';

export default class SimulationControllerClientWrapper {
  private simulationClient: SimulationControllerClient;

  constructor(simulationClient: SimulationControllerClient) {
    this.simulationClient = simulationClient;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Get_SimulationMode(...args: any): void {
    this.simulationClient.Get_SimulationMode(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  StartRealMode(...args: any): void {
    this.simulationClient.StartRealMode(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  StartSimulationMode(...args: any): void {
    this.simulationClient.StartSimulationMode(args[0], args[1]);
  }
}
