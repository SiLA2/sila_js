import { SiLAServiceClient } from '@sila-standard/core';

export default class SiLAServiceClientWrapper {
  private serviceClient: SiLAServiceClient;

  constructor(serviceClient: SiLAServiceClient) {
    this.serviceClient = serviceClient;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  GetFeatureDefinition(...args: any): void {
    this.serviceClient.GetFeatureDefinition(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Get_ImplementedFeatures(...args: any): void {
    this.serviceClient.Get_ImplementedFeatures(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Get_ServerDescription(...args: any): void {
    this.serviceClient.Get_ServerDescription(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Get_ServerName(...args: any): void {
    this.serviceClient.Get_ServerName(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Get_ServerType(...args: any): void {
    this.serviceClient.Get_ServerType(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Get_ServerUUID(...args: any): void {
    this.serviceClient.Get_ServerUUID(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Get_ServerVendorURL(...args: any): void {
    this.serviceClient.Get_ServerVendorURL(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Get_ServerVersion(...args: any): void {
    this.serviceClient.Get_ServerVersion(args[0], args[1]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  SetServerName(...args: any): void {
    this.serviceClient.SetServerName(args[0], args[1]);
  }
}
