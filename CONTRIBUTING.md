# Contributing

## Development

1. Run `npm ci` to install dependencies.

## Publishing Packages Locally

Test any changes made to the packages by publishing them to a local NPM registry.

### Prerequisites

Install Verdaccio using `npm`

```bash
npm install --global verdaccio
```

Run `verdaccio` in a second terminal. Visit `http://localhost:4873` to confirm the registry is working.

Backup your `~/.npmrc` file, since the following steps will modify it.

Configure NPM to use the local registry for all `@sila-standard` packages.

```bash
npm set "@sila-standard:registry" "http://localhost:4873"
npm adduser --registry http://localhost:4873 # Use your GitLab username and email.
```

### Publishing

Make sure that Verdaccio is running in the background. Otherwise run `verdaccio`

:warning: Always double check that your `~/.npmrc` file is configured to publish to the local registry.

Log in to your local npm registry:

```bash
npm set "@sila-standard:registry" "http://localhost:4873"
npm login --registry http://localhost:4873
```

To publish all packages run `npm publish -ws`

Confirm that the packages were published by visiting: `http://localhost:4873`

To unpublish packages run `npm unpublish -ws --force`
