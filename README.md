# SiLA 2 Node.js Implementation

SiLA 2 reference implementations in JavaScript.

## Packages

- [@sila-standard/client](./packages/client/README.md)
- [@sila-standard/core](./packages/core/README.md)
- [@sila-standard/dynamic-client](./packages/dynamic-client/README.md)
- [@sila-standard/fdl-parser](./packages/fdl-parser/README.md)
- [@sila-standard/feature](./packages/feature/README.md)
- [@sila-standard/proto-builder](./packages/proto-builder/README.md)
- [@sila-standard/server](./packages/server/README.md)
- [@sila-standard/sila-base](./packages/sila-base/README.md)
- [@sila-standard/static-client](./packages/static-client/README.md)

## Examples

- [examples](./packages/examples/README.md)

## Development

Please look at our [CONTRIBUTING.md](CONTRIBUTING.md) file to get started.
